%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Focus Beamer Presentation
% LaTeX Template
% Version 1.0 (8/8/18)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Pasquale Africa (https://github.com/elauksap/focus-beamertheme) with modifications by 
% Vel (vel@LaTeXTemplates.com)
%
% Template license:
% GNU GPL v3.0 License
%
% Important note:
% The bibliography/references need to be compiled with bibtex.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Defence Großer Beleg
% The actual content of the presentation.
% Last edited: 10/27/2020
% 
% Author:
% Lukas Gerlach (https://gitlab.com/m0nstR/defence-grosser-beleg)
%
% License (derived from Focus Beamer template):
% GNU GPL v3.0 License
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{beamer}

\usetheme{focus} % Use the Focus theme supplied with the template
% Add option [numbering=none] to disable the footer progress bar
% Add option [numbering=fullbar] to show the footer progress bar as always full with a slide count

% Uncomment to enable the ice-blue theme
% \definecolor{main}{RGB}{92, 138, 168}
% \definecolor{background}{RGB}{240, 247, 255}
\definecolor{titlecolor}{RGB}{249,138,0}
\setbeamercolor{titlelike}{fg=titlecolor, bg=main}

\definecolor{blockblue}{RGB}{0,111,249}

\newenvironment<>{definitionblock}[1][]{%
  \setbeamercolor{block title}{fg=background,bg=blockblue}%
  \setbeamercolor{block body}{fg=main,bg=blockblue!10!background}
  \begin{definition}#2}{\end{definition}}

\newenvironment<>{proposition}[1][]{%
  \begin{block}{Proposition}#2}{\end{block}}
  
\newenvironment<>{proofblock}[1][]{%
  \textit{Proof. #1}\\#2}

%------------------------------------------------

\usepackage{booktabs} % Required for better table rules
\usepackage{mathtools}
\usepackage{tikz}
\usetikzlibrary{arrows}

%----------------------------------------------------------------------------------------
%	 TITLE SLIDE
%----------------------------------------------------------------------------------------

\title{Don't Repeat Yourself:\\Termination of the Skolem Chase on Disjunctive Existential Rules}
\subtitle{}
\author{Lukas Gerlach}
\institute{Knowledge-Based Systems Group\\Technische Universit\"at Dresden, Germany}

\date{08.10.2020}



%------------------------------------------------

\begin{document}


\begin{frame}
  \maketitle
\end{frame}

\begin{frame}
  \frametitle{Motivation}
  \begin{itemize}
    \item Reasoning over \emph{Knowledge Bases}
    \pause
    \item Expressivity of \emph{Disjunctive Existential Rules}
    \pause
    \item Efficiency of the \emph{Skolem Chase} (ASP Solvers)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Knowledge Bases}

  \begin{definitionblock}
    A \emph{knowledge base} is a pair $\langle R, I \rangle$ of a rule set $R$
    and an instance $I$.
  \end{definitionblock}
  
  \pause

  \begin{definitionblock}
    An \emph{instance} is a set of function free \emph{facts} (ground atoms).
  \end{definitionblock}

  \pause

  \begin{definitionblock}
    A \emph{(disjunctive existental) rule} $\rho$ is an expression of the form
    \begin{align*}
      \forall \vec{x} \forall \vec{y}.[B_\rho(\vec{x}, \vec{y}) \to \bigvee\nolimits_{i=1}^n \exists \vec{z}_i.H_\rho^i(\vec{x}_i, \vec{z}_i)]
    \end{align*}
    where $B_\rho \text{ and } H_\rho^i$ are 
    conjunctions of atoms without function symbols or constants;
    $\vec{x}, \vec{y},$ and $\vec{z}_i$ are pairwise disjoint lists of variables; and $\bigcup_{i=1}^n \vec{x}_i = \vec{x}$.
  \end{definitionblock}

  % Let's consider some \emph{instance} $I$:
  % \begin{align*}
  %   I \coloneqq \{\, \textit{Pizza}(\textit{myPizza}) \,\}
  % \end{align*}
  % \pause
  % and a singleton \emph{rule set} $R$ containing the \emph{rule}:
  % \begin{align*}
  %   \textit{Pizza}(x) \to \textit{InFridge}(x) \lor \exists z.(\textit{Service}(z) \land \textit{Delivers}(z, x))
  % \end{align*}
  % \pause
  % We now have a \emph{knowledge base}:
  % \begin{align*}
  %   \mathcal{K} \coloneqq \langle R, I \rangle
  % \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Querying Knowledge Bases}

  \begin{definitionblock}
    A \emph{boolean conjunctive query (BCQ)} is an expression of the form
    $\exists \vec{z}.\varphi(\vec{z})$
    where $\varphi$ is a conjunction of function free atoms.
  \end{definitionblock}

  \pause

  \begin{example}
    Consider the following instance $I$, rule set $R$ and BCQ $\sigma$:
    \onslide<2->{
      \begin{align*}
        I &\coloneqq \{\, \textit{Pizza}(\textit{myPizza}) \,\}\\
        \onslide<3->{R &\coloneqq \{\, \textit{Pizza}(x) \to \textit{InFridge}(x) \lor \exists z.(\textit{Service}(z) \land \textit{Delivers}(z, x)) \,\}\\}
        \onslide<4->{\sigma &\coloneqq \exists z.(\textit{Service}(z) \land \textit{Delivers}(z, \textit{myPizza}))}
      \end{align*}
    }
    \onslide<5->{
      \centering
      Is $\sigma$ entailed by $\langle R, I \rangle$?
    }
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{How to solve this in general?}

  \begin{definitionblock}
    A BCQ $\sigma \coloneqq \exists \vec{z}.\varphi(\vec{z})$ is \emph{entailed}
    by a knowledge base $\mathcal{K}$ if, 
    for each first order model $M$ of $\mathcal{K}$,
    there exists a substitution $\theta$
    such that 
    $\varphi\theta \subseteq M$.
  \end{definitionblock}

  \pause
  \alert{Problems:}
  \begin{enumerate}
    \item The number of models may be infinite.
    \item Individual models may be infinite in size.
  \end{enumerate}

  \pause

  \begin{proposition}
    BCQ entailment is undecidable \cite{BeeriVardi1981}.
  \end{proposition}
\end{frame}

\begin{frame}<1-2>[label=univmodelset]
  \frametitle{Do we really need every model?}

  \begin{definitionblock}
    A \emph{universal model set} \cite{DisjunctiveChase} of a knowledge base $\mathcal{K}$
    is a set of models $\mathcal{U}$, such that for each 
    model $M$ for $\mathcal{K}$ there exists a 
    homomorphism from some model in $\mathcal{U}$
    to $M$.
  \end{definitionblock}

  \onslide<2->{
    \begin{proposition}
      A BCQ $\sigma \coloneqq \exists \vec{z}.\varphi(\vec{z})$ is entailed by a knowledge base $\mathcal{K}$
      iff it is entailed by each model in some universal model set of $\mathcal{K}$.
    \end{proposition}
  }
  \onslide<3->{
    We study an algorithm that should compute a finite universal model set
    containing only finite models.
  }
\end{frame}

\begin{frame}
  \frametitle{Do we really need every model?}

  \begin{proposition}
    A BCQ $\sigma \coloneqq \exists \vec{z}.\varphi(\vec{z})$ is entailed by a knowledge base $\mathcal{K}$
    iff it is entailed by each model in some universal model set of $\mathcal{K}$.
  \end{proposition}

  \begin{proofblock}
    "$\Rightarrow$":\\
    If $\sigma$ is entailed by $\mathcal{K}$, 
    then it is entailed for every model of $\mathcal{K}$.
  \end{proofblock}
\end{frame}

\begin{frame}
  \frametitle{Do we really need every model?}

  \begin{proposition}
    A BCQ $\sigma \coloneqq \exists \vec{z}.\varphi(\vec{z})$ is entailed by a knowledge base $\mathcal{K}$
    iff it is entailed by each model in some universal model set of $\mathcal{K}$.
  \end{proposition}

  \begin{proofblock}
    "$\Leftarrow$":\\
    Consider a model $M$ of $\mathcal{K}$.
    \pause
    \begin{enumerate}
      \item For each model $U$ in some universal model set $\mathcal{U}$ of $\mathcal{K}$,
        there exists a substitution $\theta$
        with $\varphi \theta \subseteq U$.
      \pause
      \item By (1): There exists a model $U \in \mathcal{U}$
        such that there exists a homomorphism $\tau$ from $U$ to $M$.
      \pause
      \item By (1) and (2): $\tau \circ \theta$ is a substitution with
        $\varphi (\tau \circ \theta) \subseteq M$.
      \pause
      \item By (3): $\sigma$ is entailed by $\mathcal{K}$.
    \end{enumerate}
    \qed
  \end{proofblock}
\end{frame}

\againframe<3>{univmodelset}

\begin{frame}[label=generalchaseprocedure]
  \frametitle{Chasing a Universal Model Set}

  \begin{block}{General Chase Procedure}
    \begin{itemize}
      \item Input: Knowledge Base $\mathcal{K}$
      \item Procedure: Apply rules in $\mathcal{K}$ exhaustively, until no new facts are obtained. 
        Consider the head disjuncts individually by branching out on them.
      \item Output: Universal Model Set of $\mathcal{K}$
    \end{itemize}
  \end{block}

  \pause

  \alert{Questions}
  \pause
  \begin{enumerate}
    \item How to apply rules?
    \pause
    \item Do we indeed obtain a finite set of finite models?
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{How to apply rules?}

  \begin{definitionblock}
    A \emph{trigger} is a pair $\lambda \coloneqq \langle \rho, \theta \rangle$
    of a rule $\rho$ and a substitution $\theta$.\\
    \pause
    In the context of a fact set $F$, the trigger $\lambda$ is:
    \pause
    \begin{itemize}
      \item \emph{active} if $B_\rho \theta \subseteq F$,
      \pause
      \item \emph{obsolete} if $sk(H_\rho^i) \theta \subseteq F$ for some $1 \leq i \leq \textit{branch}(\rho)$, \pause and
      \item \emph{applicable} to $F$ if it is active and not obsolete.
    \end{itemize}
    \pause
    If $\lambda$ is applicable to $F$,
    then the \emph{application} of $\lambda$ on $F$ is defined as the set of fact sets:
    $$\lambda(F) \coloneqq \{\, F \cup sk(H_\rho^i) \theta \mid 1 \leq i \leq \textit{branch}(\rho) \,\}$$
  \end{definitionblock}
  
  \pause

  Those applications can be implemented using ASP-solvers.
\end{frame}

\begin{frame}
  \frametitle{How to apply rules?}

  \begin{example}
    Consider the following instance and rule set:
    \begin{align*}
      I &\coloneqq \{\, \textit{Pizza}(\textit{myPizza}) \,\}\\
      \rho &\coloneqq \textit{Pizza}(x) \to \textit{InFridge}(x) \lor (\textit{Service}(f^z(x)) \land \textit{Delivers}(f^z(x), x))
    \end{align*}

    \onslide<2->{
      \centering
      \begin{tikzpicture}[auto]
        \node[shape=circle,inner sep=2pt,draw,fill=black,label=above:{$\textit{myPizza}: \textit{Pizza}$}] (A) at (-0.5, 2) {};

        \onslide<3->{
          \node[shape=circle,inner sep=2pt,draw,fill=black,label=below:{$\textit{myPizza}: \textit{Pizza}, \textit{InFridge}$}] (B) at (-3,0.5) {};
          \draw[ultra thick, ->] (-0.9,1.8) -- (-2.6,0.9);
        }
        \onslide<4->{
          \node[shape=circle,inner sep=2pt,draw,fill=black,label=above:{$\textit{myPizza}: \textit{Pizza}$}] (C) at (3,1) {};
          \draw[ultra thick, ->] (-0.1,1.9) -- (1.6,1.5);
        }
        \onslide<5->{
          \node[shape=circle,inner sep=2pt,draw,fill=black,label=below:{$f^z(\textit{myPizza}): \textit{Service}$}] (D) at (3,0) {};
        }
        \onslide<6->{
          \path [>=angle 45,thick, <-](C) edge node {$\textit{Delivers}$} (D);
        }

      \end{tikzpicture}
    }

  \end{example}
  \onslide<7->{
    \centering
    The BCQ $\exists z.(\textit{Service}(z) \land \textit{Delivers}(z, \textit{myPizza}))$ is \alert{not} entailed.
  }
\end{frame}

\againframe<4>{generalchaseprocedure}

\begin{frame}
  \frametitle{Do we indeed obtain a finite set of finite models?}

  \alert{Not always:} The chase may not terminate on a knowledge base.

  \pause

  \begin{definitionblock}
    A knowledge base $\mathcal{K}$ is \emph{terminating}
    if no new facts can be obtained at some point in the chase computation.\\
    \pause
    A rule set $R$ is \emph{terminating} if $\langle R, I \rangle$ 
    is terminating for every instance $I$.
  \end{definitionblock}

  \pause

  \alert{Problem:} Knowledge base termination and rule set termination are undecidable \cite{BeeriVardi1981,Deutsch2008bb,ChaseUndecidable}.

  \pause

  \begin{example}
    The following rule set is \alert{not} terminating.
    \begin{align*}
      \textit{Pizza}(x) &\to \textit{Last}(x) \lor (\textit{NextOrder}(x, f^z(x)) \land \textit{Pizza}(f^z(x)))
    \end{align*}
  \end{example}
  
\end{frame}

\begin{frame}
  \frametitle{Better safe than sorry}

  \begin{definitionblock}
    \emph{Acyclicity Notions} are sufficient conditions for rule set termination.
  \end{definitionblock}

  \pause

  Existing notions:

  \begin{itemize}
    \item \textsf{MFA} \cite{MFA} - Skolem Chase
    \item \textsf{RMFA} \cite{RMFA} - Restricted Chase
  \end{itemize}

  \pause

  Why not use \textsf{MFA}?

  \pause

  \textsf{MFA} is \alert{not defined} for rule sets with disjunctions.\\
  \hfill\\

  \pause

  Why not use \textsf{RMFA}?

  \pause

  \textsf{RMFA} is \alert{not sound} for the disjunctive skolem chase. (We can still use some ideas.)
\end{frame}

\begin{frame}
  \frametitle{Better safe than sorry}

  General idea of \textsf{MFA}: Compute chase on critical instance $I_R^\star$ and check for cyclic terms.
  
  \pause

  \begin{theorem}
    A rule set $R$ without disjunctions is terminating
    if and only if $\langle R, I_R^\star \rangle$ is terminating \cite{SchemaMappings}.
  \end{theorem}

  \pause

  This approach does \alert{not} work for disjunctive existential rules.
  
  \pause

  \begin{example}
    Consider the rule set $R$ and its critical instance $I_R^\star$:
    \begin{align*}
      R &= \{\, \textit{Pizza}(x) \to \textit{Last}(x) \lor (\textit{NextOrder}(x, f^z(x)) \land \textit{Pizza}(f^z(x))) \,\}\\
      I_R^\star &= \{\, \textit{Pizza}(\star), \textit{Last}(\star), \textit{NextOrder}(\star, \star) \,\}
    \end{align*}
    The knowledge base $\langle R, I_R^\star \rangle$ is terminating but $R$ is not.
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{Naive Fix}

  Treat disjunctions as conjunctions.

  \pause
  
  \begin{proposition}
    Consider a rule set $R$ and a rule set $R'$ that results
    from $R$ by replacing disjunctions with conjunctions.
    If $R'$ is terminating, then $R$ is terminating.
  \end{proposition}

  \pause

  Though, this is not satisfactory.

  \pause

  \begin{example}
    The following rule set is terminating but it does not terminate if we replace the disjunction by a conjunction:
    \begin{align*}
      \textit{Pizza}(x) &\to \textit{Last}(x) \lor (\textit{NextOrder}(x, f^z(x)) \land \textit{Pizza}(f^z(x)))\\
      \textit{NextOrder}(y, x) &\to \textit{Last}(x)
    \end{align*}
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{Generalise Obsoleteness}

  How to improve upon the naive fix?\\
  \hfill\\

  \pause

  \alert{Problem:} Obsoleteness is too restrictive.\\
  Can we identify triggers that are obsolete independent of the starting instance?

  \pause

  \begin{definitionblock}
    A trigger $\langle \rho, \theta \rangle$ is \emph{blocked} if it is obsolete w.r.t. to the set of facts
    that are "necessarily involved" in the derivation of $B_\rho\theta$.
  \end{definitionblock}

  \pause

  \begin{theorem}
    In the context of a rule set:
    If a trigger $\lambda$ is blocked, then
    $\lambda$ is not applicable to any fact set 
    occurring in the chase on any instance.
  \end{theorem}
\end{frame}

% \begin{frame}
%   \frametitle{Generalise obsoleteness}

%   What does \emph{necessarily involved} mean?
%   Important: We assume that existentially quantified variables 
%   do not reoccur across rules (w.l.o.g.).
%   We denote the single rule containing the existentially quantified variable $z$ by
%   $\textit{rule}(z)$.

%   \begin{block}{Definition}
%     For a term $t$, we define the fact set $F_t$ inductively.
%     If $t$ is a constant, then $F_t \coloneqq \emptyset$.
%     Otherwise, $t$ is of the form $f^z(\vec{s})$.
%     We define the substitution $\theta^t$ 
%     on the variables in the body of $\textit{rule}(z)$
%     that maps the frontier $\vec{x}$ to $\vec{s}$
%     and $\vec{y}$ to fresh constants $\vec{c^t_y}$.
%     We define $F_t$ to be the smallest fact set such that
%     \begin{itemize}
%       \item $B_{\textit{rule}(z)} \theta^t \subseteq F_t$,
%       \item $sk(H_{\textit{rule}(z)}^k(\vec{x}_k, \vec{z}_k))\theta^t \subseteq F_t$
%         for the $1 \leq k \leq \textit{branch}(\textit{rule}(z))$ with $z \in \vec{z}_k$, and 
%       \item $F_{s} \subseteq F_t$ for every term $s \in \vec{s}$.
%     \end{itemize}
%   \end{block}
% \end{frame}

\begin{frame}
  \frametitle{Generalise Obsoleteness}

  What does "necessarily involved" mean?

  \pause

  \begin{example}
    Consider the following rule set:
    \begin{align*}
      \rho_1: \textit{Pizza}(x) &\to \textit{Last}(x) \lor (\textit{NextOrder}(x, f^z(x)) \land \textit{Pizza}(f^z(x)))\\
      \textit{NextOrder}(y, x) &\to \textit{Last}(x)
    \end{align*}
    Consider the trigger $\langle \rho_1, \{\, x \mapsto f^z(\star) \,\} \rangle$.

    \pause
    
    To derive $f^z(\star)$, $\langle \rho_1, \{\, x \mapsto \star \,\} \rangle$ was applied
    before.

    \pause

    The following fact set has necessarily been involved:
    $$\{\, Pizza(\star), \pause NextOrder(\star, f^z(\star)), \pause Pizza(f^z(\star)), \pause \alert{Last(f^z(\star))} \,\}$$
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{Compute a generalized Chase result}

  \begin{definitionblock}
    For a rule set $R$, we define $\mathsf{DMFA}(R)$ to be the smallest fact set
    such that $I_R^\star \subseteq \mathsf{DMFA}(R)$ and,
    for every trigger $\langle \rho, \theta \rangle$ 
    with $\rho \in R$
    that is active w.r.t. $\mathsf{DMFA}(R)$ and not blocked,
    we have
    $sk(H_\rho^i)\theta \subseteq \mathsf{DMFA}(R)$ 
    for all $1 \leq i \leq \textit{branch}(\rho)$.
  \end{definitionblock}

  \pause

  \begin{definitionblock}
    $R$ is \textsf{DMFA} if $\mathsf{DMFA}(R)$ does not contain a cyclic term.
  \end{definitionblock}
  
  \pause

  \begin{theorem}
    If a rule set $R$ is \textsf{DMFA}, then $R$ is terminating.
  \end{theorem}
\end{frame}
\begin{frame}
  \frametitle{Compute a generalized Chase result}

  \begin{theorem}
    If a rule set $R$ is \textsf{DMFA}, then $R$ is terminating.
  \end{theorem}

  \begin{proofblock}
    We show that 
    $R$ is not \textsf{DMFA} if $R$ is not terminating.\\
    \pause
    Let $\sigma$ be a mapping over constants with $\sigma(c) \coloneqq \star$\\for all constants $c$.
    \pause
    \begin{enumerate}
      \item There exists an instance $I$ such that $\langle R, I \rangle$ 
        is not terminating.
      \pause
      \item By (1): The chase sequence
        of $\langle R, I \rangle$ contains a cyclic term $t$.
      \pause
      \item By (2): The cyclic term
        $\sigma(t)$ is in $\mathsf{DMFA}(R)$ (via induction over the chase sequence).
      \pause
      \item By (3): $R$ is not \textsf{DMFA}.
    \end{enumerate}
    \qed
  \end{proofblock}
\end{frame}

\begin{frame}
  \frametitle{Compute a generalized Chase result}

  \begin{example}
    $R$:
    \begin{align*}
      \textit{Pizza}(x) &\to \textit{Last}(x) \lor (\textit{NextOrder}(x, f^z(x)) \land \textit{Pizza}(f^z(x)))\\
      \textit{NextOrder}(y, x) &\to \textit{Last}(x)
    \end{align*}

    \onslide<2->{

      $\mathsf{DMFA}(R)$:

      \centering

      \begin{tikzpicture}[auto]
        \onslide<2->{
          \node[shape=circle,inner sep=2pt,draw,fill=black,label=below:{$\star: \textit{Pizza}, \textit{Last}$}] (A) at (0,0) {};
          \path [>=angle 45,thick, ->](A) edge[distance=10mm] node[above] {$\textit{NextOrder}$} (A);
        }

        \onslide<3->{
          \node[shape=circle,inner sep=2pt,draw,fill=black,label=above:{$f^z(\star): \textit{Pizza}\onslide<4->{, \textit{Last}}$}] (B) at (3,1) {};
          \path [>=angle 45,thick, <-](B) edge node {$\textit{NextOrder}$} (A);
        }

        \onslide<5->{
          \node[shape=circle,inner sep=2pt,draw,fill=gray,label=below:{$f^z(f^z(\star)): \textit{Pizza}$}] (C) at (6,0) {};
          \path [>=angle 45,thick, ->, dashed](B) edge node {$\textit{NextOrder}$} (C);
        }
      \end{tikzpicture}
    }
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{Hierarchy of Acyclicity Notions}

  \centering
  \begin{tikzpicture}
    \onslide<1->{
      \node[align=center] at (0,3.5) {\textbf{Disjunctive Existential Rule Sets}};
      \draw[rounded corners, very thick] (-5, 0) rectangle (5, 4);
    }

    \onslide<2->{
      \node[align=center] at (-2.5,2.5) {Terminating w.r.t. \\ restricted chase};
      \draw[rounded corners, very thick] (-5, 0) rectangle (5, 3);
    }

    \onslide<3->{
      \node[align=center] at (-2.5,1) {Terminating w.r.t. \\ disjunctive skolem chase};
      \draw[rounded corners, very thick] (-5, 0) rectangle (5, 2);
    }

    \onslide<4->{
      \node at (2.5,2.5) {\textsf{RMFA}};
      \draw[rounded corners, very thick] (0, 0) rectangle (5, 3);
    }

    \onslide<5->{
      \node at (3,0.5) {\textsf{MFA}};
      \draw[rounded corners, very thick] (1, 0) rectangle (5, 1);
    }

    \onslide<6->{
      \node at (3,1.5) {\textbf{\textsf{DMFA}}};
      \draw[rounded corners, very thick] (1, 0) rectangle (5, 2);
    }
  \end{tikzpicture}

  \only<7->{
    \begin{example}
      The following singleton rule set is contained in the blank space:
      $$P(y, x) \land Q(y) \to \exists z.(P(x, z) \land P(z, x))$$
    \end{example}
  }
\end{frame}

\begin{frame}
  \frametitle{Complexity of the \textsf{DMFA} Check}

  \begin{theorem}
    Checking if a rule set $R$ is \textsf{DMFA} is \textsc{2ExpTime}-complete.
  \end{theorem}
  
  \pause

  \begin{proofblock}[(Sketch)]
    (Membership): We compute $\mathsf{DMFA}(R)$ step by step. \pause      
      The number of facts without cyclic terms is at most doubly exponential. \pause
      Therefore, there are at most doubly exponentially many steps of which each is computable
      in \textsc{2ExpTime}.
    
    \pause
    
    (Hardness): Reduction from \textsf{MFA} (\textsf{MFA} and \textsf{DMFA} coincide for rule sets without disjunctions) \cite{MFA,Cali}.
  \end{proofblock}
\end{frame}
\begin{frame}
  \frametitle{Complexity of BCQ entailment for \textsf{DMFA} Ruleset}

  \begin{theorem}
    Let $R$ be a rule set that is \textsf{DMFA}
    and let $\mathcal{K}$ be a knowledge base featuring $R$.
    BCQ entailment for $\mathcal{K}$ is \textsc{coN2ExpTime}-complete.
  \end{theorem}

  \pause

  \begin{proofblock}[(Sketch)]
    (Membership): We consider BCQ non-entailment. \pause 
    The number of 
    sets of fact sets is at most triply exponential. \pause
    Since it suffices to guess one fact set in each chase step,
    we end up in \textsc{N2ExpTime} by using a similar step by step computation
    as for the DMFA check. \pause
    Thus, BCQ entailment is in \textsc{coN2ExpTime}.
    
    \pause

    (Hardness): Reduction from the word problem of \textsc{N2ExpTime}-bounded turing machines similar to
    the proof for \textsf{RMFA} \cite{RMFA,Cali}.
  \end{proofblock}
\end{frame}

\begin{frame}
  \frametitle{Results}

  \begin{itemize}
    \item \textsf{DMFA} is a novel acyclicity notions tailored towards the disjunctive skolem chase.
    \pause
    \item \textsf{DMFA} is in between \textsf{MFA} and \textsf{RMFA} (as expected).
    \pause
    \item Checking \textsf{DMFA} is \textsc{2ExpTime}-complete and checking BCQ entailment for a rule set that is \textsf{DMFA} is \textsc{coN2ExpTime}-complete.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Future Work}

  \textbf{In theory:}
  \begin{enumerate}
    \item Develop a cyclicity notion for the disjunctive skolem chase.
    \pause
    \item Refine notions to capture as many rule sets as possible.
  \end{enumerate}

  \pause

  \textbf{In practice:}
  \begin{enumerate}
    \item Evaluate \textsf{DMFA} on real world knowledge bases.
    \pause
    \item Use an ASP based implementation of the disjunctive skolem chase
      for reasoning with description logics.
  \end{enumerate}
\end{frame}

\appendix

% \begin{frame}
%   \frametitle{Triggers}

%   \begin{definitionblock}
%     A pair $\lambda \coloneqq \langle \rho, \theta \rangle$ of a rule $\rho$ 
%     and a substitution $\theta$ that is defined exactly on the universally quantified
%     variables in $\rho$
%     is called \emph{trigger}.
%     In the context of a fact set $F$:
%     \begin{itemize}
%       \item The trigger $\lambda$ is \emph{active} if $B_\rho \theta \subseteq F$.
%       \item The trigger $\lambda$ is \emph{obsolete} if $sk(H_\rho^i) \theta \subseteq F$ for some $1 \leq i \leq \textit{branch}(\rho)$.
%       \item The trigger $\lambda$ is \emph{applicable} to $F$ if it is active and it is not obsolete.
%     \end{itemize}
%     If $\lambda$ is applicable to $F$,
%     then the \emph{application} of $\lambda$ on $F$ is defined as the set of fact sets
%     $\lambda(F) \coloneqq \{\, F \cup sk(H_\rho^i) \theta \mid 1 \leq i \leq \textit{branch}(\rho) \,\}$.
%   \end{definitionblock}
% \end{frame}

% \begin{frame}
%   \frametitle{Disjunctive Skolem Chase}

%   \begin{definitionblock}
%     Let $R$ be a rule set, 
%     $F$ a fact set and 
%     $\mathcal{F}$ a set of fact sets.
%     \begin{itemize}
%       \item The \emph{application} of $R$ on $F$ is defined 
%         as the set of fact sets
%         \[
%           R(F) \coloneqq 
%           \begin{cases}
%             \{\, F \,\}, &\text{if } \Lambda^F_R = \emptyset\\
%             \bigcup_{\lambda \in \Lambda^F_R} \lambda(F), &\text{otherwise}
%           \end{cases}
%         \]
%         where $\Lambda^F_R$ is the set of all triggers
%         using rules in $R$
%         that are applicable to $F$.

%       \item The \emph{application} of $R$ on $\mathcal{F}$ is the set of fact sets
%         $R(\mathcal{F}) \coloneqq \bigcup_{F \in \mathcal{F}} R(F)$.
%     \end{itemize}
%   \end{definitionblock}
% \end{frame}

% \begin{frame}
%   \frametitle{Disjunctive Skolem Chase}

%   \begin{definitionblock}
%     Let $R$ be a rule set, 
%     $F$ a fact set and 
%     $\mathcal{F}$ a set of fact sets.
%     \begin{itemize}
%       \item The \emph{saturating application} of $R_\text{dlog}$ 
%         on $F$, written $R_\text{dlog}^*(F)$, is defined as 
%         the smallest superset of the fact set $F$
%         such that $R_\text{dlog}(R_\text{dlog}^*(F)) = \{\, R_\text{dlog}^*(F) \,\}$.
      
%       \item The \emph{saturating application} of $R_\text{dlog}$ 
%         on $\mathcal{F}$ is defined 
%         as the set of fact sets
%         $R_\text{dlog}^*(\mathcal{F}) \coloneqq \{\, R_\text{dlog}^*(F) \mid F \in \mathcal{F} \,\}$.
%     \end{itemize}
%   \end{definitionblock}

%   \begin{definitionblock}
%     The \emph{chase sequence} 
%     of a knowledge base $\mathcal{K} \coloneqq \langle R, I \rangle$ 
%     is the sequence of sets of fact sets 
%     $\mathcal{F}_\mathcal{K}^0, \mathcal{F}_\mathcal{K}^1, \dots$ 
%     defined inductively via 
%     $\mathcal{F}_\mathcal{K}^0 := \{\, I \,\}$ and
%     $\mathcal{F}_\mathcal{K}^{i} \coloneqq R(R_\text{dlog}^*(\mathcal{F}_\mathcal{K}^{i-1}))$ for all $i > 0$.
%   \end{definitionblock}
% \end{frame}

% \begin{frame}
%   \frametitle{What does necessarily involved mean?}

%   \begin{definitionblock}
%     Let $t$ be a ground term
%     defined using constants in $\mathsf{Const}$ and
%     functions symbols that occur in the skolemized
%     rules of a rule set $R$.
%     We define the fact set $F_t$ inductively.
%     If $t \in \mathsf{Const}$, then $F_t \coloneqq \emptyset$.
%     Otherwise, $t$ is of the form $f^z(\vec{s})$.
%     We define the substitution $\theta^t$ 
%     on the variables in $B_{\textit{rule}(z)}(\vec{x}, \vec{y})$
%     that maps the frontier $\vec{x}$ to $\vec{s}$
%     and $\vec{y}$ to fresh constants $\vec{c^t_y}$ 
%     that are unique for $t$ and the variables in $\vec{y}$.
%     We define $F_t$ to be the smallest fact set such that
%     \begin{itemize}
%       \item $B_{\textit{rule}(z)} \theta^t \subseteq F_t$,
%       \item $sk(H_{\textit{rule}(z)}^k(\vec{x}_k, \vec{z}_k))\theta^t \subseteq F_t$
%         for the $1 \leq k \leq \textit{branch}(\textit{rule}(z))$ with $z \in \vec{z}_k$, and 
%       \item $F_{s} \subseteq F_t$ for every term $s \in \vec{s}$.
%     \end{itemize}
%   \end{definitionblock}
% \end{frame}

% \begin{frame}
%   \frametitle{Blockedness}

%   \begin{definitionblock}
%     A trigger $\langle \rho, \theta \rangle$ 
%     is \emph{weakly blocked} in the context of a rule set $R$ 
%     if $\rho \notin R_\text{dlog}$ 
%     and for some $1 \leq k \leq \textit{branch}(\rho)$,
%     $sk(H_\rho^k)\theta \subseteq F_{\rho, \theta}$
%     where $F_{\rho, \theta} \coloneqq R_\text{dlog}^*(B_\rho\theta \cup \bigcup \{\, F_t \mid t \text{ is a term in } B_\rho\theta \,\})$.
%   \end{definitionblock}
% \end{frame}

% \begin{frame}
%   \frametitle{What we do not cover}

%   \begin{example}
%     This rule set is terminating w.r.t. the disjunctive skolem chase
%     but not \textsf{DMFA} and even not \textsf{RMFA}.
%     \begin{align*}
%       P(y, x) &\to Q(x) \lor \exists z.R(x, z)\\
%       Q(y) \land R(y, x) &\to P(y, x)
%     \end{align*}
%   \end{example}
% \end{frame}

\begin{frame}[allowframebreaks]{References}
  \nocite{*} % Display all references regardless of if they were cited
  \bibliography{reference.bib}
  \bibliographystyle{apalike}
\end{frame}

\begin{frame}{Technical References}
  The sources of the presentation can be found on Gitlab:\\
  \url{https://gitlab.com/m0nstR/defence-grosser-beleg}\\
  \hfill\\
  The presentation uses the Focus Theme for Latex Beamer\\
  \url{https://github.com/elauksap/focus-beamertheme}\\
  which was obtained
  from Latex Templates\\
  \url{http://www.latextemplates.com/template/focus-presentation}
\end{frame}

\end{document}
