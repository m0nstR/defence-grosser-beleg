## 05.10.2020

Questions:
- already read through beleg?
- everything fine with defence forms? -> yes
- citations? -> do it

Remarks:

- put in citations
- more boxes
- slides too empty
- more formal detail -> definition envs, some proof sketches, complexity results
- general structure: 
  - BCQ Entailment over knowledge bases
  - definitions for KB and BCQ
  - examples for KB and BCQ
  - definition of bcq entailment
  - definition of univ model set
  - proof proposition for universal model set and bcq entailment
  - -> we construct algorithm that constructs finite amount of finite models
  - describe chase formally (top down) (maybe make query part of the input)
  - define triggers with properties and applicability formally
  - explain skolemization using example
  - example for disjunctive branching with two applications (e.g. with 2 pizzas in instance)
  - example why obsoleteness is useful (merge with previous?)
  - maybe: complexity result for checking applicability
  - show bcq entailment with obtained univ model set?
  - definition for term, rule set term
  - example for termination/non-termination
  - why are we interested in rule set termination?
  - mention that disjunctive skolem chase is useful because of ASP solvers
  - mention MFA and RMFA and why they don't work
  - maybe mention restricted chase briefly
  - why MFA approach does not work (result from marnette)
  - failing example
  - fix by generalising obsoleteness (blockedness definition)
  - maybe do blockedness more formal?
  - step by step example for blockedness
  - define dmfa
  - example for dmfa(r)
  - proof dmfa result
  - hirachy of acyclicity notions with colors possibly
  - include example for blank space
  - complexity results with proof sketches
  - sum up results
  - future work (including that we want an asp based implementation for description logics)

## 07.10.2020

- add motivation again -> done
- beta and eta instead of B_\rho and H_\rho^i ??? -> keep as is
- 2: align instead of itemize -> done
- 3: (also in general) cite before full stop -> done
- 4: devide into more slides -> done
- 4: (also in general) no proof env -> done
- 4: "<-": enum step by step -> done
- 4: we don't construct an algorithm -> better study -> done
- 5: describe branching out already here -> done
- 5: ASP solvers could be mentioned here -> on next slide -> done
- 6: branch(\rho) not defined -> just explain it -> done
- 6: note on skolemization -> for example just use skolemized rules from there on (or even earlier) -> done
- 7: (also in general) no proof env -> done
- 7: remove that slide (mention ASP solver somewhere else) -> done
- 8: show tree if possible -> done
- 10: k term. if chase leads to finite universal model set with finite models
- 12: cite before full stop -> done
- 14: improve theorem -> done
- 15: write text before align to fill space -> done
- 16: use more overlays -> done
- 16: no proof env -> done
- 17: maybe add f^z(f^z(\star)) and cross it out then or something like this -> done
- 19: no proof env -> done
- 20: no proof env -> done
- 20: rephrase proofs -> done
- 21: use more overlays -> done
