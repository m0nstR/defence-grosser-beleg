# Defence Slides of my "Großer Beleg"

This repo contains the slides that I used for the defence of a university module called "Großer Beleg".  
A prebuilt version of the slides is also contained in this repo (`build/defence-lukas-gerlach.pdf`).  
The slides are based on the [Focus Theme](https://github.com/elauksap/focus-beamertheme) for Latex Beamer.  
The template has been downloaded from [Latex Templates](http://www.latextemplates.com/template/focus-presentation).

## License

The [Focus Theme](https://github.com/elauksap/focus-beamertheme) is released under the [GNU GPL v3.0 License](https://www.gnu.org/licenses/gpl-3.0.en.html).  
Therefore, this software is released under the [GNU GPL v3.0 License](https://www.gnu.org/licenses/gpl-3.0.en.html) as well.
